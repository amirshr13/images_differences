# images_differences
This project is a python script that shows the differences in two images.

# Prerequisites
install these packages by following commands:

`pip install scikit-image`
`pip install pip install opencv-python`
`pip install imutils`
`pip install argparse`

# Arguments
-f --first is the path to first image
-s --second is the path to second image

# Example usage
python image_difference.py -f tree_one.jpg -s tree_two.jpg

