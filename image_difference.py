from skimage.measure import compare_ssim
import imutils
import cv2
import argparse


def preprocess(first_image, second_image):
    first_img = cv2.imread(first_image)
    second_img = cv2.imread(second_image)

    first_img = cv2.resize(first_img, (600, 400))
    second_img = cv2.resize(second_img, (600, 400))

    first_gray = cv2.cvtColor(first_img, cv2.COLOR_RGB2GRAY)
    second_gray = cv2.cvtColor(second_img, cv2.COLOR_RGB2GRAY)

    score, diff = compare_ssim(first_gray, second_gray, full=True)
    diff = (diff * 255).astype('uint8')
    print('SSIM: {}'.format(score))

    thresh = cv2.threshold(diff, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)

    for c in cnts:
        x, y, w, h = cv2.boundingRect(c)
        cv2.rectangle(first_img, (x, y), (x + w, y + h), (0, 0, 255), 2)
        cv2.rectangle(second_img, (x, y), (x + w, y + h), (0, 0, 255), 2)

    cv2.imwrite('image.jpg', first_img)
    cv2.imshow('First image with differences', first_img)
    cv2.imshow('Second image with differences', second_img)

    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return first_img


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--first', required=True, help='path to first image')
    parser.add_argument('-s', '--second', required=True, help='path to second image')
    parser = vars(parser.parse_args())

    preprocess(parser['first'], parser['second'])
